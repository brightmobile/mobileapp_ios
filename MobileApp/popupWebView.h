//
//  popupWebView.h
//  ServicePI
//
//  Created by Евгений Малаховский on 28/10/2018.
//  Copyright © 2018 Евгений Малаховский. All rights reserved.
//

#import <WebKit/WebKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface popupWebView : UIView <WKUIDelegate, WKNavigationDelegate>
+(popupWebView*)createPopupWebView:(UIView*)parent configuration:(WKWebViewConfiguration *)configuration forNavigationAction:(WKNavigationAction *)navigationAction windowFeatures:(WKWindowFeatures *)windowFeatures;

@property WKWebView *webView;
@property UIActivityIndicatorView *indicator;
@end

NS_ASSUME_NONNULL_END
