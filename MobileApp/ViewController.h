//
//  ViewController.h
//  AppForSale2
//
//  Created by Евгений Малаховский on 23.08.16.
//  Copyright © 2016 Евгений Малаховский. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MAWebView.h"
#import "LeftButton.h"

@interface ViewController : UIViewController<WKNavigationDelegate>

@property NSString *url;

-(void)prepareWeb;
-(void)loadURL;

@property MAWebView *webView;
@property (weak, nonatomic) IBOutlet UIView *viewForWeb;

@property (weak, nonatomic) IBOutlet LeftButton *leftMenu;

@end

