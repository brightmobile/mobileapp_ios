//
//  webView.h
//  AppForSale2
//
//  Created by Евгений Малаховский on 27.08.16.
//  Copyright © 2016 Евгений Малаховский. All rights reserved.
//

#import <WebKit/WebKit.h>

@interface MAWebView : WKWebView <WKNavigationDelegate,WKUIDelegate>

-(void)loadURL:(NSString*)urlStr;

@property UIViewController *controller;

@property (nonatomic, strong) WKWebViewConfiguration * webConfig;

@property BOOL isLeftMenu;

+ (WKProcessPool *)pool;

-(void)showIndicator;
-(void)hideIndicator;

-(void)prepareNextController;
-(UIViewController*)getNextController;

@end
