//
//  LeftButton.h
//  LeftMenu
//
//  Created by Евгений Малаховский on 22.02.16.
//  Copyright © 2016 Евгений Малаховский. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LeftButton : UIBarButtonItem

-(void)refresh;

@end
