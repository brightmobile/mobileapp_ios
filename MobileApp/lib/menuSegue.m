//
//  afsSegue.m
//  AppForSale
//
//  Created by Евгений Малаховский on 04.02.16.
//  Copyright © 2016 MLab. All rights reserved.
//

#import "menuSegue.h"

@implementation menuSegue

- (void)perform{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"goPushMenu" object:self.destinationViewController];
}

@end
