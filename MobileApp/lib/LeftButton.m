//
//  LeftButton.m
//  LeftMenu
//
//  Created by Евгений Малаховский on 22.02.16.
//  Copyright © 2016 Евгений Малаховский. All rights reserved.
//

#import "LeftButton.h"
#import "DrawerController.h"

static BOOL isVisable;

@implementation LeftButton

-(UINavigationController*)navController{
    UIViewController *parentViewController = [[[UIApplication sharedApplication] delegate] window].rootViewController;
    while (parentViewController.presentedViewController != nil){
        parentViewController = parentViewController.presentedViewController;
    }
    UINavigationController *navController = parentViewController;
    
    if ([navController isKindOfClass:[DrawerController class]]){
        DrawerController *ctrl2 = parentViewController;
        navController = ctrl2.centerViewController;
    }

    if ([navController isKindOfClass:[UITabBarController class]]){
        UITabBarController *ctrl3 = navController;
        navController = ctrl3.selectedViewController;
    }

    if ([navController isKindOfClass:[UINavigationController class]]){
        NSArray *ctrls = navController.viewControllers;
        if (ctrls.count>1)
            return navController;
    }
    return nil;
}

-(void)refresh{
    self.enabled = isVisable;
    
    if ([self navController])
        [self setImage:[[UIImage imageNamed:@"Back"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate]];
    else
        [self setImage:[[UIImage imageNamed:@"Menu"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate]];
}

-(instancetype)initWithCoder:(NSCoder *)aDecoder{
    self = [super initWithCoder:aDecoder];
    
    self.action = @selector(goLeftButton);
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(enableMenu)
                                                 name:@"enableMenu"
                                               object:nil];
    
    return self;
}

-(void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

-(void)enableMenu{
    isVisable = true;
    self.enabled = isVisable;
}

-(void)goLeftButton{
    if ([self navController])
        [[self navController] popViewControllerAnimated:YES];
    else
        [[NSNotificationCenter defaultCenter] postNotificationName:@"goLeftButton" object:nil];
}

@end
