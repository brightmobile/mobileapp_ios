//
//  LeftController.m
//  AppForSale2
//
//  Created by Евгений Малаховский on 13.09.16.
//  Copyright © 2016 Евгений Малаховский. All rights reserved.
//

#import "LeftController.h"

@interface LeftController ()

@end

@implementation LeftController
@synthesize webView;

-(UIStatusBarStyle)preferredStatusBarStyle{
    return UIStatusBarStyleLightContent;
}

-(void)viewDidLoad{
    [super viewDidLoad];
    
    CGRect frame = self.view.bounds;
    frame.size.width = frame.size.width * 0.9;
    webView = [[MAWebView alloc] initWithFrame:frame];
    [self.view addSubview:webView];
    webView.alpha = 0;
    
    [webView.scrollView setBounces:NO];
    
    webView.isLeftMenu = true;
    webView.controller = self;
    
    NSDictionary *setting = [NSDictionary dictionaryWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"Info" ofType:@"plist"]];

    NSString *changeLeftMenu = [[NSUserDefaults standardUserDefaults] objectForKey:@"changeLeftMenu"];
    
    if (changeLeftMenu)
        [webView loadURL:changeLeftMenu];
    else
        [webView loadURL:[setting[@"SiteUrl"] stringByAppendingString:@"/left.php"]];
    
    if ([[UIDevice currentDevice].systemVersion floatValue]>=11){
        webView.scrollView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
    }
}

@end
