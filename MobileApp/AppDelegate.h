//
//  AppDelegate.h
//  AppForSale2
//
//  Created by Евгений Малаховский on 23.08.16.
//  Copyright © 2016 Евгений Малаховский. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end

