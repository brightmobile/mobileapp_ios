//
//  MAScriptMessageHandler.h
//  mobileapp
//
//  Created by Евгений Малаховский on 27.09.2018.
//  Copyright © 2018 Евгений Малаховский. All rights reserved.
//

#import <WebKit/WebKit.h>

@interface MAScriptMessageHandler : NSObject <WKScriptMessageHandler>

+(MAScriptMessageHandler*)shared;

@end
