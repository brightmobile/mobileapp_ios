//
//  MAScriptMessageHandler.m
//  mobileapp
//
//  Created by Евгений Малаховский on 27.09.2018.
//  Copyright © 2018 Евгений Малаховский. All rights reserved.
//

#import "MAScriptMessageHandler.h"
#import "core.h"
#import <QRCodeReaderViewController/QRCodeReader.h>
#import <QRCodeReaderViewController/QRCodeReaderViewController.h>
//#import <Contacts/Contacts.h>
#import "MAWebView.h"
#import "ViewController.h"

#define StoryBoard [UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]]

@implementation MAScriptMessageHandler{
    ViewController *nextController;
}

static MAScriptMessageHandler *sharedMySingleton = NULL;
+(MAScriptMessageHandler*)shared {
    if (!sharedMySingleton) {
        sharedMySingleton = [MAScriptMessageHandler new];
    }
    return sharedMySingleton;
}

#pragma mark -WKScriptMessageHandler
- (void)userContentController:(WKUserContentController *)userContentController didReceiveScriptMessage:(WKScriptMessage *)message{
    
    NSData *temp = [message.body dataUsingEncoding:NSUTF8StringEncoding];
    NSDictionary *body = [NSJSONSerialization JSONObjectWithData:temp options:0 error:nil];
    
    if ([body isKindOfClass:[NSDictionary class]]){
        NSString *funcName = body[@"funcName"];
        NSDictionary *params = body[@"params"];
        MAWebView *webView = (MAWebView*)message.webView;
        
        NSLog(@"Функция [%@]: %@", [webView.URL.absoluteString lastPathComponent], funcName);
        
        if ([funcName isEqualToString:@"loadPageStart"]){
            UINavigationController *navCtrl = [StoryBoard instantiateViewControllerWithIdentifier:@"Center"];
            
            ViewController *nextController = navCtrl.viewControllers[0];
            [[core shared] setColors:nextController];
            nextController.url = params[@"url"];
            nextController.title = params[@"title"];
            
            [[NSNotificationCenter defaultCenter] postNotificationName:@"goPushMenu" object:navCtrl];
        }
        else if ([funcName isEqualToString:@"loadPageBlank"]){
            nextController = [webView getNextController];
            if (!nextController){
                nextController = [StoryBoard instantiateViewControllerWithIdentifier:@"ViewController"];
                [nextController prepareWeb];
            }
            
            [[core shared] setColors:nextController];
            
            nextController.url = params[@"url"];
            [nextController.webView loadURL:nextController.url];
            nextController.title = params[@"title"];
            
            UINavigationController *navigationController = [[core shared] rootController].navigationController;
            [navigationController pushViewController:nextController animated:YES];
        }
        else if ([funcName isEqualToString:@"closeMenu"]){
            [[NSNotificationCenter defaultCenter] postNotificationName:@"closeMenu" object:nil];
        }
        else if ([funcName isEqualToString:@"enableMenu"]){
            [[NSNotificationCenter defaultCenter] postNotificationName:@"enableMenu" object:nil];
        }
        else if ([funcName isEqualToString:@"closeController"]){
            [[[core shared] rootController].navigationController popViewControllerAnimated:YES];
        }
        else if ([funcName isEqualToString:@"reload"]){
            [webView showIndicator];
            [webView reload];
        }
        else if ([funcName isEqualToString:@"onCustomEvent"]){
            [[NSNotificationCenter defaultCenter] postNotificationName:@"onCustomEvent" object:params];
        }
        else if ([funcName isEqualToString:@"setColors"]){
            [[NSUserDefaults standardUserDefaults] setObject:params forKey:@"setColors"];
            [[NSUserDefaults standardUserDefaults] synchronize];
            [[core shared] setColors:webView.controller];
        }
        else if ([funcName isEqualToString:@"setServer"]){
            UINavigationController *navCtrl = [StoryBoard instantiateViewControllerWithIdentifier:@"Center"];
            ViewController *nextController = navCtrl.viewControllers[0];
            [[core shared] setColors:nextController];
            nextController.url = params[@"url"];
            nextController.title = params[@"title"];
            [[NSNotificationCenter defaultCenter] postNotificationName:@"goPushMenu" object:navCtrl];
            
            [[NSNotificationCenter defaultCenter] postNotificationName:@"changeLeftMenu" object:[params[@"url"] stringByAppendingString:@"/left.php"]];
        }
        else if ([funcName isEqualToString:@"getCurrentPosition"]){
            [[core shared] getGPS:^(CLLocation *gps){
                NSString *func = [NSString stringWithFormat:@"app.callBackExecute(%@, {coords: {latitude: %f, longitude: %f, accuracy: %f}, timestamp: %f})"
                                  ,params[@"success"]
                                  ,gps.coordinate.latitude
                                  ,gps.coordinate.longitude
                                  ,gps.horizontalAccuracy
                                  ,[gps.timestamp timeIntervalSince1970]
                                  ];
                [message.webView evaluateJavaScript:func completionHandler:^(id obj,NSError *error){
                }];
            } error:^(NSString *error) {
                NSString *func = [NSString stringWithFormat:@"app.callBackExecute(%@, {code: %d, message: \"%@\"})"
                                  ,params[@"error"]
                                  ,1
                                  ,@"PERMISSION_DENIED"
                                  ];
                [message.webView evaluateJavaScript:func completionHandler:^(id obj,NSError *error){
                    //NSLog(@"");
                }];
            } continueUpdate:0];
        }
        else if ([funcName isEqualToString:@"closeBar"]){
            [[[core shared] rootController].navigationController setNavigationBarHidden:YES animated:NO];
            [webView setFrame:webView.superview.bounds];
        }
        else if ([funcName isEqualToString:@"openBar"]){
            [[[core shared] rootController].navigationController setNavigationBarHidden:NO];
        }
        else if ([funcName isEqualToString:@"getQRCode"]){
            QRCodeReader *reader = [QRCodeReader readerWithMetadataObjectTypes:@[AVMetadataObjectTypeQRCode]];
            QRCodeReaderViewController *vc = [QRCodeReaderViewController readerWithCancelButtonTitle:@"Закрыть" codeReader:reader startScanningAtLoad:YES showSwitchCameraButton:YES showTorchButton:YES];
            vc.modalPresentationStyle = UIModalPresentationFormSheet;
            vc.delegate = self;
            [reader setCompletionWithBlock:^(NSString *resultAsString){
                 [reader stopScanning];
                 [webView.controller dismissViewControllerAnimated:NO completion:NULL];
                 NSString *JS = [NSString stringWithFormat:@"app.callBackExecute(%@, {CODE: '%@'})"
                                 ,params[@"callback"]
                                 ,resultAsString];
                 [webView evaluateJavaScript:JS completionHandler:^(id obj,NSError *error){
                      NSLog(@"");
                  }];
             }];
            [webView.controller presentViewController:vc animated:YES completion:NULL];
        }
        else if ([funcName isEqualToString:@"toggleMenu"]){
            [[NSNotificationCenter defaultCenter] postNotificationName:@"toggleMenu" object:nil];
        }
//        else if ([funcName isEqualToString:@"getContacts"])
//        {
//            __block CNContactStore *store = [[CNContactStore alloc] init];
//            __block NSMutableDictionary *contactss = [[NSMutableDictionary alloc] init];
//
//            [store requestAccessForEntityType:CNEntityTypeContacts completionHandler:^(BOOL granted, NSError * _Nullable error) {
//                //
//                if (granted == YES)
//                {
//                    //keys with fetching properties
//                    NSArray *keys = @[CNContactBirthdayKey,CNContactFamilyNameKey, CNContactGivenNameKey, CNContactPhoneNumbersKey, CNContactImageDataKey, CNContactEmailAddressesKey];
//                    NSString *containerId = store.defaultContainerIdentifier;
//                    NSPredicate *predicate = [CNContact predicateForContactsInContainerWithIdentifier:containerId];
//                    NSError *error;
//                    NSArray *cnContacts = [store unifiedContactsMatchingPredicate:predicate keysToFetch:keys error:&error];
//                    if (error) {
//                        NSLog(@"error fetching contacts %@", error);
//                    } else {
//                        NSString *phone;
//                        NSString *fullName;
//                        NSString *firstName;
//                        NSString *lastName;
//                        NSMutableArray *contactNumbersArray;
//
//                        for (CNContact *contact in cnContacts) {
//                            firstName = contact.givenName;
//                            lastName = contact.familyName;
//
//                            if (lastName == nil) {
//                                fullName=[NSString stringWithFormat:@"%@",firstName];
//                            }else if (firstName == nil){
//                                fullName=[NSString stringWithFormat:@"%@",lastName];
//                            }
//                            else{
//                                fullName=[NSString stringWithFormat:@"%@ %@",firstName,lastName];
//                            }
//
//                            for (CNLabeledValue *label in contact.phoneNumbers) {
//                                phone = [label.value stringValue];
//                                if ([phone length] > 0) {
//                                    [contactNumbersArray addObject:phone];
//                                }
//                            }
//
//                            [contactss setObject:phone forKey:fullName];
//                        }
//                        dispatch_async(dispatch_get_main_queue(), ^{
//                            NSError * err;
//                            NSData * jsonData = [NSJSONSerialization dataWithJSONObject:contactss options:0 error:&err];
//                            NSString * myString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
//                            NSString *JS = [NSString stringWithFormat:@"app.callBackExecute(%@, {CODE: '%@'})"
//                                            ,params[@"callback"]
//                                            ,myString];
//                            [webView evaluateJavaScript:JS completionHandler:^(id obj,NSError *error)
//                             {
//                                 NSLog(@"%@", error);
//                             }];
//                        });
//
//                    }
//                }
//            }];
//        }
        else if ([funcName isEqualToString:@"copy"]){
            [UIPasteboard generalPasteboard].string = params[@"text"];;
        }
        else if ([funcName isEqualToString:@"functionNative"]){
            NSString *JS = [NSString stringWithFormat:@"app.callBackExecute(%@, {CODE: '%@'})"
                            ,params[@"callback"]
                            ,@"Возвращаемый результат из функции"];
            [webView evaluateJavaScript:JS completionHandler:nil];
        }
        else if ([funcName isEqualToString:@"openDocument"]){
            NSURL *urlOpenDocument = [NSURL URLWithString:params[@"url"]];
            UIApplication *app = [UIApplication sharedApplication];
            if ([app canOpenURL:urlOpenDocument])
                [app openURL:urlOpenDocument];
        }
        else{
            NSLog(@"Неизвестная функция: %@", funcName);
        }
    }
}

- (void)activeAfterSetting:(NSNotification*)notification{
    
}

- (void)readerDidCancel:(QRCodeReaderViewController *)reader{
    [reader dismissViewControllerAnimated:YES completion:NULL];
}

@end
