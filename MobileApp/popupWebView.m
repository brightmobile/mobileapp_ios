//
//  popupWebView.m
//  ServicePI
//
//  Created by Евгений Малаховский on 28/10/2018.
//  Copyright © 2018 Евгений Малаховский. All rights reserved.
//

#import "popupWebView.h"

@implementation popupWebView

+(popupWebView*)createPopupWebView:(UIView*)parent configuration:(WKWebViewConfiguration *)configuration forNavigationAction:(WKNavigationAction *)navigationAction windowFeatures:(WKWindowFeatures *)windowFeatures{
    
    popupWebView *backgroundView = [[popupWebView alloc] initWithFrame:parent.bounds];
    backgroundView.backgroundColor = [UIColor blackColor];
    backgroundView.alpha = 0.7f;
    [parent addSubview:backgroundView];
    
    backgroundView.webView = [[WKWebView alloc] initWithFrame:CGRectInset(parent.bounds, 16, 16) configuration:configuration];
    
    [parent addSubview:backgroundView.webView];
    [backgroundView.webView loadRequest:navigationAction.request];
    
    backgroundView.webView.UIDelegate = backgroundView;
    backgroundView.webView.navigationDelegate = backgroundView;

    backgroundView.webView.alpha = 0;

    if (!backgroundView.indicator){
        backgroundView.indicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
        backgroundView.indicator.color = [UIColor whiteColor];

        backgroundView.indicator.frame = parent.frame;
        backgroundView.indicator.hidesWhenStopped=YES;

        [parent addSubview:backgroundView.indicator];
    }
    [backgroundView showIndicator];
    
    return backgroundView;
}

- (void)webView:(WKWebView *)webView didFinishNavigation:(null_unspecified WKNavigation *)navigation{
    [self hideIndicator];
}

- (void)webViewDidClose:(WKWebView *)webView API_AVAILABLE(macosx(10.11), ios(9.0)){
    [webView removeFromSuperview];
    [self.indicator removeFromSuperview];
    [self removeFromSuperview];
}

-(void)showIndicator{
    [self.indicator startAnimating];
    [self.superview addSubview:self.indicator];
}

-(void)hideIndicator{
    [self.indicator stopAnimating];
    
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.3f];
    [UIView setAnimationCurve:UIViewAnimationCurveLinear];
    
    self.webView.alpha = 1;
    [UIView commitAnimations];
}

@end
